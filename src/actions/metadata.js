import { GET_METADATA } from './types'

export const getMetadata = () => {
    return {
        type: GET_METADATA,
        payload: {
            request: {
                method: 'GET',
                url: '/metadata'
            }
        }
    }
}
