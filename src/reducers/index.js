import { combineReducers } from 'redux'
import { reducer as reduxFormReducer } from 'redux-form'
import { connectRouter } from 'connected-react-router'
import { createBrowserHistory } from 'history'
import { getMetadataLoading } from './metadata'

const history = createBrowserHistory()

const error = (state = null, action) => {
    const { type, error } = action
    if (error && error.response && error.response.data) {
        return { message: error.response.data }
    }
    if (error) {
        if (/^@@/.test(action.type)) {
            if (typeof error === 'string') {
                return { message: error }
            } else {
                return { message: `Error: ${action.type}` }
            }
        }
        if (typeof error === 'string') {
            return { message: error }
        } else {
            return { message: 'Something went wrong' }
        }
    }
    if (type === 'CLEAR_ERROR_MESSAGE') {
        return null
    }
    return state
}

// login reducer
import metadata from './metadata'

// export root reducer
export const rootReducer = combineReducers({
    form: reduxFormReducer,
    router: connectRouter(history),
    error,
    metadata
})

// export history
export { history }

// export selectors
export { getMetadataLoading }
