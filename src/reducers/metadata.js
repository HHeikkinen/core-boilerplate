import {
    GET_METADATA,
    GET_METADATA_SUCCESS,
    GET_METADATA_FAIL
} from 'actions/types'
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

const initialState = {
    loading: false
}

function metadata(state = initialState, action) {
    switch (action.type) {
        case GET_METADATA:
            return {
                ...state,
                loading: true
            }
        case GET_METADATA_SUCCESS:
            return {
                ...action.payload.data,
                loading: false
            }
        case GET_METADATA_FAIL:
            return {
                ...state,
                loading: false
            }
        default:
            return state
    }
}

const persistConfig = {
    key: 'metadata',
    storage,
    blacklist: ['loading']
}

export const getMetadataLoading = state =>
    (state ? state.metadata : initialState).loading

export default persistReducer(persistConfig, metadata)
