import React, { Component, Fragment } from 'react'
import { BrowserRouter as Router, Link, Route } from 'react-router-dom'
import AuthChecker from '../Authentication/AuthChecker'
import { connect } from 'react-redux'

class Navigation {
    constructor(route, name, icon, active = true) {
        this.route = route
        this.name = name
        this.active = active
        this.icon = icon
    }
}

class Nav extends Component {
    renderNav = (nav, i) => {
        if (!nav) return null
        const current = nav.route === this.props.currentRoute
        const content = (
            <Fragment>
                <span className="material-icons">{nav.icon}</span>
                <span>{nav.name}</span>
            </Fragment>
        )
        if (!nav.active) {
            return (
                <div
                    key={`${i}_${nav.route}`}
                    className={`main-nav-link-container inactive${
                        current ? ' current' : ''
                    }`}
                >
                    {content}
                </div>
            )
        }
        return (
            <Link
                key={i}
                className={`main-nav-link-container active${
                    current ? ' current' : ''
                }`}
                to={`/${nav.route}`}
            >
                {content}
            </Link>
        )
    }

    render() {
        const navigation = [new Navigation('home', 'Home', 'home')]
        return (
            <AuthChecker>
                {({ isAuthenticated }) =>
                    isAuthenticated ? (
                        <div className={`main-nav-container`}>
                            <nav>
                                <div className="main-nav-title-container">
                                    <Link to="/">corenet</Link>
                                </div>
                                <div className="main-nav-links">
                                    {navigation.map(this.renderNav)}
                                </div>
                                <div className="filler" />
                                {/* <button
                                    type="text"
                                    className="nav-button main-nav-logout"
                                >
                                    Log out
                                </button> */}
                            </nav>
                        </div>
                    ) : null
                }
            </AuthChecker>
        )
    }
}

export default connect(
    state => ({}),
    dispatch => ({
        logout: () => null //NOTIMP
    })
)(Nav)
