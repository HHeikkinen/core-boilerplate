import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import AuthChecker from './AuthChecker'

const ProtectedRoute = ({ component: Component, render, ...rest }) => {
    return (
        <AuthChecker>
            {({ isAuthenticated }) => (
                <Route
                    {...rest}
                    render={props =>
                        isAuthenticated ? (
                            Boolean(Component) ? (
                                <Component {...props} />
                            ) : (
                                render(props)
                            )
                        ) : (
                            <Redirect to="/login" />
                        )
                    }
                />
            )}
        </AuthChecker>
    )
}

export default ProtectedRoute
