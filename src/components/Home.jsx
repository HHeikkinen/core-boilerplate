import React, { Component } from 'react'

export default class Home extends Component {
    render() {
        return (
            <div className="home-page-container">
                <h1>Hello world</h1>
                <div>This here is the core boilerplate</div>
            </div>
        )
    }
}
