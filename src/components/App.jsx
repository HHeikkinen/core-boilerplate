import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect
} from 'react-router-dom'
import { Helmet } from 'react-helmet'

// components
import Nav from './Header/Nav'
import Home from './Home'

class App extends Component {
    componentDidMount() {}

    render() {
        return (
            <div>
                <Helmet titleTemplate="%s - Core" defaultTitle="Core">
                    <meta name="description" content="Core boilerplate app" />
                </Helmet>
                <Switch>
                    <Route
                        path="/:route"
                        render={routeProps => {
                            const route = routeProps.match.params.route
                            return (
                                <div className="app-container">
                                    <Nav currentRoute={route} />
                                    <div className="app-content-container">
                                        <Switch>
                                            <Route
                                                path="/home"
                                                component={Home}
                                            />
                                            <Redirect to="/" />
                                        </Switch>
                                    </div>
                                </div>
                            )
                        }}
                    />
                    <Redirect to="/home" />
                </Switch>
            </div>
        )
    }
}

export default connect()(App)
